const maskCpf = number => {
  if (!number) return '';
  let newNumber = number.replace(/\D/g, '');
  newNumber = newNumber.substring(0, 11);

  newNumber = newNumber.replace(/^(\d{3})(\d)/g, '$1.$2');

  newNumber = newNumber.replace(/^(\d{3}.\d{3})(\d)/g, '$1.$2');

  newNumber = newNumber.replace(/^(\d{3}.\d{3}.\d{3})(\d)/g, '$1-$2');

  return newNumber;
};

export default maskCpf;
