import React, {useState} from 'react';
import {withRouter} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SideBar from '../SideBar';
import {getLoggedUser, setLoggedUser} from "../../localStorage";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

const ButtonAppBar = ({ history }) => {
    const classes = useStyles();
    const [shouldOpenSideBar, setShouldOpenSideBar] = useState(false);
    const loggerUser = getLoggedUser();
    const onSignOut = () => {
        setLoggedUser('');
        history.push('/login');
    };

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    {!!loggerUser && (
                        <>
                            <IconButton
                                edge="start"
                                className={classes.menuButton}
                                color="inherit"
                                aria-label="menu"
                                onClick={() => setShouldOpenSideBar(true)}
                            >
                                <MenuIcon/>
                            </IconButton>

                            <Typography variant="h6" className={classes.title}>
                                Árvores de conhecimentos
                            </Typography>
                            <Button onClick={onSignOut} color="inherit">Sair</Button>
                        </>
                    )}
                </Toolbar>
            </AppBar>
            {shouldOpenSideBar && (
                <SideBar onClose={() => setShouldOpenSideBar(false)}/>
            )}
        </div>
    );
};

export default withRouter(ButtonAppBar);