import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
    Link
} from "react-router-dom";
import PeopleIcon from '@material-ui/icons/People';
import StarIcon from '@material-ui/icons/Star';
import AssignmentIcon from '@material-ui/icons/Assignment';
import WorkIcon from '@material-ui/icons/Work';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
});

const menuList = [{
    label: 'Funcionários',
    route: '/funcionarios',
    icon: <PeopleIcon />,
},{
    label: 'Competências',
    route: '/competencias',
    icon: <StarIcon />,
},{
    label: 'Setores',
    route: '/setores',
    icon: <AssignmentIcon />,
},{
    label: 'Cargos',
    route: '/cargos',
    icon: <WorkIcon />,
}];

const SideBar = ({ onClose }) => {
    const classes = useStyles();

    const sideList = () => (
        <div
            className={classes.list}
            role="presentation"
            onClick={onClose}
        >
            <List>
                {menuList.map(item => (
                    <ListItem button key={item.label} component={props => <Link to={item.route} {...props} />}>
                        <ListItemIcon>{item.icon}</ListItemIcon>
                        <ListItemText primary={item.label} />
                    </ListItem>
                ))}
            </List>
            <Divider />
            <List>
                <ListItem button>
                    <ListItemIcon><CloseIcon /></ListItemIcon>
                    <ListItemText primary="Fechar" />
                </ListItem>
            </List>
        </div>
    );

    return (
        <div>
            <Drawer open onClose={onClose}>
                {sideList()}
            </Drawer>
        </div>
    );
};

export default SideBar;