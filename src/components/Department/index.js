import React from 'react';
import {withRouter} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '../Button';
import {EmptyTableMessage, PageTitle} from '../styles';
import {listDepartments} from "../../localStorage";

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflow: 'auto',
        maxHeight: 'calc(100vh - 200px)',
    },
});

const DepartmentTable = ({history}) => {
    const classes = useStyles();
    const departments = listDepartments();

    const onEdit = id => history.push(`/setores/${id}`);

    return (
        <>
            <PageTitle>Lista de Setores</PageTitle>
            <Button label="Novo setor" margin="0 0 20px 0" onClick={() => history.push('/novo-setor')}/>
            <Paper className={classes.root}>
                <Table aria-label="simple table">
                    <TableBody>
                        {departments.map(row => (
                            <TableRow key={row.id} onClick={() => onEdit(row.id)}>
                                <TableCell component="th" scope="row">
                                    <div>{row.name}</div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {!departments.length && <EmptyTableMessage>Nenhum setor cadastrado</EmptyTableMessage>}
            </Paper>
        </>
    );
};

export default withRouter(DepartmentTable);