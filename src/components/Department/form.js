import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import {PageTitle} from "../styles";
import Button from "../Button";
import {addDepartment, updateDepartment, listDepartments, deleteDepartment} from "../../localStorage";

const DepartmentForm = ({history}) => {
    const [idToUpdate, setIdToUpdate] = useState(null);
    const [name, setName] = useState('');
    const [error, setError] = useState('');

    useEffect(() => {
        const url = window.location.pathname;
        const id = parseInt(url.substring(url.lastIndexOf('/') + 1), 10);
        if (id) {
            const departments = listDepartments();
            const found = departments.find(item => item.id === id);
            if (found) {
                setIdToUpdate(found.id);
                setName(found.name);
            } else
                history.push('/setores');
        }
    }, []);

    const onSaveDepartment = () => {
        if (!name) return setError('Campo inválido.');
        if (idToUpdate) updateDepartment(idToUpdate, name);
        else addDepartment(name);
        history.push('/setores');
    };

    const onDelete = () => {
        deleteDepartment(idToUpdate);
        history.push('/setores');
    };

    return (
        <>
            <PageTitle>{`${idToUpdate ? 'Editar' : 'Novo'} setor`}</PageTitle>
            <div>
                <TextField
                    autoFocus
                    fullWidth
                    label="Nome"
                    variant="filled"
                    value={name}
                    onChange={event => {
                        setError('');
                        setName(event.target.value);
                    }}
                    error={!!error}
                    helperText={error}
                />
                <Button label="Salvar" margin="40px 0 0 0" width="100%" onClick={onSaveDepartment}/>
                {!!idToUpdate && (
                    <Button label="Excluir" variant="outlined" margin="20px 0 0 0" width="100%" onClick={onDelete}/>
                )}
            </div>
        </>
    );
};

export default withRouter(DepartmentForm);