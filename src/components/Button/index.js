import React from 'react';
import Button from '@material-ui/core/Button';
import styled from "styled-components";

export const StyledButton = styled(Button)`
    && {
        width: ${({width}) => width};
        margin: ${({margin}) => margin};
    }
`;


export default function ButtonComponent({label, onClick, margin, width, variant}) {
    return (
        <StyledButton variant={variant} color="primary" onClick={onClick} margin={margin} width={width}>
            {label}
        </StyledButton>
    );
};

ButtonComponent.defaultProps = {
    variant: 'contained',
};