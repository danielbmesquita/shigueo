import React from 'react';
import {withRouter} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '../Button';
import {EmptyTableMessage, PageTitle} from '../styles';
import {listEmployees} from "../../localStorage";
import maskCpf from "../../maskCpf";

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflow: 'auto',
        maxHeight: 'calc(100vh - 200px)',
    },
});

const PeopleTable = ({history}) => {
    const classes = useStyles();
    const employees = listEmployees();
    const onEdit = id => history.push(`/funcionarios/${id}`);

    return (
        <>
            <PageTitle>Lista de Funcionários</PageTitle>
            <Button label="Novo funcionário" margin="0 0 20px 0" onClick={() => history.push('/novo-funcionario')}/>
            <Paper className={classes.root}>
                <Table aria-label="simple table">
                    <TableBody>
                        {employees.map(row => (
                            <TableRow key={row.id} onClick={() => onEdit(row.id)}>
                                <TableCell component="th" scope="row">
                                    <div>{row.name}</div>
                                    <div>{row.email}</div>
                                    <div>{maskCpf(row.document)}</div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {!employees.length && <EmptyTableMessage>Nenhum funcionário cadastrado</EmptyTableMessage>}
            </Paper>
        </>
    );
};

export default withRouter(PeopleTable);