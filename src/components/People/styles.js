import styled from "styled-components";

export const StyledEmployeeWrapper = styled.div`
    height: calc(100vh - 76px);
    overflow: auto;
`;
