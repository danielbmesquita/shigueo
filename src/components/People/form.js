import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {PageTitle} from "../styles";
import Button from "../Button";
import {
    addEmployee,
    updateEmployee,
    listEmployees,
    deleteEmployee,
    listDepartments,
    listJobTitles,
    getLoggedUser
} from "../../localStorage";
import {StyledEmployeeWrapper} from './styles';
import maskCpf from "../../maskCpf";
import removeNotNumberCharacters from "../../removeNotNumberCharacters";
import CompetencesModal from "./modal";

const useStyles = makeStyles(theme => ({
    formControl: {
        width: '100%',
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const PeopleForm = ({history}) => {
    const classes = useStyles();
    const isCreatingNewAccount = window.location.pathname === '/nova-conta';

    const [idToUpdate, setIdToUpdate] = useState(null);
    const [name, setName] = useState('');
    const [document, setDocument] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [idDepartment, setIdDepartment] = useState('');
    const [idJobTitle, setIdJobTitle] = useState('');
    const [error, setError] = useState('');

    useEffect(() => {
        const url = window.location.pathname;
        const id = parseInt(url.substring(url.lastIndexOf('/') + 1), 10);
        if (id) {
            const employees = listEmployees();
            const found = employees.find(item => item.id === id);
            if (found) {
                setIdToUpdate(found.id);
                setName(found.name);
                setDocument(found.document);
                setEmail(found.email);
                setPassword(found.password);
                setIdDepartment(found.idDepartment || '');
                setIdJobTitle(found.idJobTitle || '');
            } else
                history.push('/funcionarios');
        }
    }, []);

    const onSave = () => {
        const isValid = !name || !document || !email || !password;
        if (isValid) return setError('Preencha os campos obrigatórios.');
        if (idToUpdate) updateEmployee({idToUpdate, name, document, email, password, idDepartment, idJobTitle});
        else addEmployee({name, document, email, password, idDepartment, idJobTitle});
        history.push(isCreatingNewAccount ? '/login' : '/funcionarios');
    };

    const onDelete = () => {
        deleteEmployee(idToUpdate);
        history.push('/funcionarios');
    };

    const departments = listDepartments();
    const jobTitles = listJobTitles();
    const loggedUser = getLoggedUser();

    return (
        <StyledEmployeeWrapper>
            <PageTitle>
                {isCreatingNewAccount ? 'Nova conta' : `${idToUpdate ? 'Editar' : 'Novo'} funcionário`}
            </PageTitle>
            <div>
                <TextField
                    autoFocus
                    fullWidth
                    label="Nome"
                    variant="filled"
                    value={name}
                    onChange={event => {
                        setError('');
                        setName(event.target.value);
                    }}
                    error={!!error}
                />
                <TextField
                    fullWidth
                    label="CPF"
                    variant="filled"
                    value={maskCpf(document)}
                    onChange={event => {
                        setError('');
                        if (event.target.value.length <= 14)
                            setDocument(removeNotNumberCharacters(event.target.value));
                    }}
                    error={!!error}
                />
                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel id="department-select">Departamento</InputLabel>
                    <Select
                        labelId="department-select"
                        id="department-select"
                        value={idDepartment}
                        onChange={event => setIdDepartment(event.target.value)}
                    >
                        <MenuItem value={null}>
                            <em>None</em>
                        </MenuItem>
                        {departments.map(department => (
                            <MenuItem key={department.id} value={department.id}>{department.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel id="job-title-select">Cargo</InputLabel>
                    <Select
                        labelId="job-title-select"
                        id="job-title-select"
                        value={idJobTitle}
                        onChange={event => setIdJobTitle(event.target.value)}
                    >
                        <MenuItem value={null}>
                            <em>None</em>
                        </MenuItem>
                        {jobTitles.map(jobTitle => (
                            <MenuItem key={jobTitle.id} value={jobTitle.id}>{jobTitle.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <TextField
                    fullWidth
                    label="E-mail"
                    variant="filled"
                    value={email}
                    onChange={event => {
                        setError('');
                        setEmail(event.target.value);
                    }}
                    error={!!error}
                />
                <TextField
                    fullWidth
                    type="password"
                    label="Senha"
                    variant="filled"
                    value={password}
                    onChange={event => {
                        setError('');
                        setPassword(event.target.value);
                    }}
                    error={!!error}
                    helperText={error}
                />

                <Button
                    label={isCreatingNewAccount ? 'Criar' : 'Salvar'}
                    margin="40px 0 0 0"
                    width="100%"
                    onClick={onSave}
                />
                {!!idToUpdate && (
                    <CompetencesModal idToUpdate={idToUpdate}/>
                )}
                {(!!idToUpdate && loggedUser.id !== idToUpdate) && (
                    <Button label="Excluir" variant="outlined" margin="20px 0 0 0" width="100%" onClick={onDelete}/>
                )}
            </div>
        </StyledEmployeeWrapper>
    );
};

export default withRouter(PeopleForm);