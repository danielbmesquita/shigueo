import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Button from "../Button";
import {listCompetences} from "../../localStorage";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DeleteIcon from '@material-ui/icons/Delete';
import {EmptyTableMessage} from "../styles";
import {listEmployeeCompetences, addCompetenceToEmployee, deleteEmployeeCompetence} from "../../localStorage";

const useStyles = makeStyles(theme => ({
    appBar: {
        position: 'relative',
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    formControl: {
        width: '100%',
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const CompetencesModal = ({idToUpdate}) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [employeeCompetences, setEmployeeCompetences] = React.useState([]);

    const updateCompetenceList = () => {
        const list = listEmployeeCompetences();
        const filteredList = list.filter(listItem => listItem.idEmployee === idToUpdate);
        setEmployeeCompetences(filteredList);
    };

    const handleClose = () => setOpen(false);
    const saveCompetence = event => {
        if (employeeCompetences.find(item => item.idCompetence === event.target.value && item.idEmployee === idToUpdate)) {
            return alert('Já adicionado.');
        }
        addCompetenceToEmployee(event.target.value, idToUpdate);
        updateCompetenceList();
    };

    const onDelete = id => {
        deleteEmployeeCompetence(id);
        updateCompetenceList();
    };

    useEffect(() => {
        if (open) {
            updateCompetenceList();
        }
    }, [open]);

    const competencesFromLocalStorage = listCompetences();

    return (
        <div>
            <Button
                label="Visualizar competências"
                margin="20px 0 0 0"
                width="100%"
                onClick={() => setOpen(true)}
            />
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon/>
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            Competências
                        </Typography>
                    </Toolbar>
                </AppBar>

                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel id="add-competence-select">Adicionar competência</InputLabel>
                    <Select
                        labelId="add-competence-select"
                        id="add-competence-select"
                        value={''}
                        onChange={saveCompetence}
                    >
                        {competencesFromLocalStorage.map(item => (
                            <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <List>
                    {employeeCompetences.map(item => (
                        <React.Fragment key={item.id}>
                            <ListItem>
                                <ListItemText
                                    primary={competencesFromLocalStorage.find(comp => comp.id === item.idCompetence).name}
                                />
                                <DeleteIcon onClick={() => onDelete(item.id)}/>
                            </ListItem>
                            <Divider/>
                        </React.Fragment>
                    ))}
                    {!employeeCompetences.length &&
                    <EmptyTableMessage>Nenhuma competência associada</EmptyTableMessage>}
                </List>
            </Dialog>
        </div>
    );
};

export default CompetencesModal;
