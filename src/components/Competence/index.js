import React from 'react';
import {withRouter} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '../Button';
import {EmptyTableMessage, PageTitle} from '../styles';
import {listCompetences} from "../../localStorage";
import KnowledgeTreeModal from "./modal";

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflow: 'auto',
        maxHeight: 'calc(100vh - 200px)',
    },
});

const CompetenceTable = ({history}) => {
    const classes = useStyles();
    const competences = listCompetences();
    const onEdit = id => history.push(`/competencias/${id}`);

    return (
        <>
            <PageTitle>Lista de Competências</PageTitle>
            <Button label="Nova competência" margin="0 0 20px 0" onClick={() => history.push('nova-competencia')}/>
            <KnowledgeTreeModal competences={competences}/>
            <Paper className={classes.root}>
                <Table aria-label="simple table">
                    <TableBody>
                        {competences.map(row => (
                            <TableRow key={row.id} onClick={() => onEdit(row.id)}>
                                <TableCell component="th" scope="row">
                                    <div>{row.name}</div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {!competences.length && <EmptyTableMessage>Nenhuma competência cadastrada</EmptyTableMessage>}
            </Paper>
        </>
    );
};

export default withRouter(CompetenceTable);
