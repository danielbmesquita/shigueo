import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import styled from "styled-components";
import Button from "../Button";
import {listEmployeeCompetences} from "../../localStorage";

const useStyles = makeStyles(theme => ({
    appBar: {
        position: 'relative',
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Container = styled.div`
    display: flex;
    flex-wrap: wrap-reverse;
    justify-content: space-around;
`;

const StyledItem = styled.div`
    flex: 0 0 30%;
    background: lightgreen;
    padding: 1%;
    border-radius: 10px;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 40px;
    flex-direction: column;
`;

export const StyledCompetenceName = styled.div`
    font-weight: 600;
`;

export const StyledCompetenceOccurrences = styled.div`
    font-size: 14px;
`;

const KnowledgeTreeModal = ({competences}) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClose = () => setOpen(false);
    const employeeCompetences = listEmployeeCompetences();
    const getOccurrences = idToFilter => {
        const number = employeeCompetences.filter(item => item.idCompetence === idToFilter).length;
        return `${number} ${number === 1 ? 'ocorrência' : 'ocorrências'}`;
    };

    return (
        <>
            <Button
                label="Ver árvore"
                margin="0 0 20px 20px"
                onClick={() => setOpen(true)}
            />
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon/>
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            Árvore
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Container>
                    {competences.map(competence => (
                        <StyledItem key={competence.id}>
                            <StyledCompetenceName>{competence.name}</StyledCompetenceName>
                            <StyledCompetenceOccurrences>{getOccurrences(competence.id)}</StyledCompetenceOccurrences>
                        </StyledItem>
                    ))}
                </Container>
            </Dialog>
        </>
    );
};

export default KnowledgeTreeModal;
