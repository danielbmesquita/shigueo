import React, {useState} from 'react';
import {withRouter} from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import Button from "../Button";
import {StyledForm, LoginWrapper, LoginTitle} from './styles';
import {setLoggedUser} from "../../localStorage";
import {listEmployees} from "../../localStorage";

export const Login = ({history}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const onSignIn = () => {
        if (!email || !password) return setError('Email e/ou senha inválidos.');
        const employees = listEmployees();
        const found = employees.find(item => item.email === email && item.password === password);

        if (found) {
            setLoggedUser(JSON.stringify(found));
            return history.push('/funcionarios');
        }

        return setError('Email e/ou senha inválidos.')
    };

    return (
        <LoginWrapper>
            <LoginTitle>Árvores de Conhecimentos</LoginTitle>
            <StyledForm noValidate autoComplete="off">
                <TextField
                    fullWidth
                    label="E-mail"
                    variant="filled"
                    type="email"
                    value={email}
                    onChange={event => {
                        setError('');
                        setEmail(event.target.value);
                    }}
                    error={!!error}
                />
                <TextField
                    fullWidth
                    label="Senha"
                    variant="filled"
                    type="password"
                    value={password}
                    onChange={event => {
                        setError('');
                        setPassword(event.target.value)
                    }}
                    error={!!error}
                    helperText={error}
                />
                <Button label="Entrar" margin="40px 0 0 0" onClick={onSignIn}/>
                <Button
                    variant="outlined"
                    label="Criar conta"
                    margin="20px 0 0 0"
                    onClick={() => history.push('/nova-conta')}
                />
            </StyledForm>
        </LoginWrapper>
    );
};

export default withRouter(Login);
