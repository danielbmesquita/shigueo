import styled from "styled-components";

export const LoginWrapper = styled.div`
    height: calc(100vh - 76px);
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

export const LoginTitle = styled.div`
    color: #3f51b5;
    font-size: 20px;
    font-weight: 600;
    margin-bottom: 40px;
    text-transform: uppercase;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
`;

export const StyledForm = styled.form`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
`;
