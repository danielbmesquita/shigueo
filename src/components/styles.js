import styled from "styled-components";

export const EmptyTableMessage = styled.div`
    margin: 20px 10px;
`;

export const PageTitle = styled.div`
    font-size: 20px;
    font-weight: 500;
    margin-bottom: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid #CCC;
`;