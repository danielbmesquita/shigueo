import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import {PageTitle} from "../styles";
import Button from "../Button";
import {addJobTitle, updateJobTitle, listJobTitles, deleteJobTitle} from "../../localStorage";

const JobTitleForm = ({history}) => {
    const [idToUpdate, setIdToUpdate] = useState(null);
    const [name, setName] = useState('');
    const [error, setError] = useState('');

    useEffect(() => {
        const url = window.location.pathname;
        const id = parseInt(url.substring(url.lastIndexOf('/') + 1), 10);
        if (id) {
            const jobTitles = listJobTitles();
            const found = jobTitles.find(item => item.id === id);
            if (found) {
                setIdToUpdate(found.id);
                setName(found.name);
            } else
                history.push('/cargos');
        }
    }, []);

    const onSave = () => {
        if (!name) return setError('Campo inválido.');
        if (idToUpdate) updateJobTitle(idToUpdate, name);
        else addJobTitle(name);
        history.push('/cargos');
    };

    const onDelete = () => {
        deleteJobTitle(idToUpdate);
        history.push('/cargos');
    };

    return (
        <>
            <PageTitle>{`${idToUpdate ? 'Editar' : 'Novo'} cargo`}</PageTitle>
            <div>
                <TextField
                    autoFocus
                    fullWidth
                    label="Nome"
                    variant="filled"
                    value={name}
                    onChange={event => {
                        setError('');
                        setName(event.target.value);
                    }}
                    error={!!error}
                    helperText={error}
                />
                <Button label="Salvar" margin="40px 0 0 0" width="100%" onClick={onSave}/>
                {!!idToUpdate && (
                    <Button label="Excluir" variant="outlined" margin="20px 0 0 0" width="100%" onClick={onDelete}/>
                )}
            </div>
        </>
    );
};

export default withRouter(JobTitleForm);