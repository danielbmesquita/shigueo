import React from 'react';
import {withRouter} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '../Button';
import {EmptyTableMessage, PageTitle} from '../styles';
import {listJobTitles} from "../../localStorage";

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflow: 'auto',
        maxHeight: 'calc(100vh - 200px)',
    },
});

const JobTitleTable = ({history}) => {
    const classes = useStyles();
    const jobTitles = listJobTitles();
    const onEdit = id => history.push(`/cargos/${id}`);

    return (
        <>
            <PageTitle>Lista de Cargos</PageTitle>
            <Button label="Novo cargo" margin="0 0 20px 0" onClick={() => history.push('/novo-cargo')}/>
            <Paper className={classes.root}>
                <Table aria-label="simple table">
                    <TableBody>
                        {jobTitles.map(row => (
                            <TableRow key={row.id} onClick={() => onEdit(row.id)}>
                                <TableCell component="th" scope="row">
                                    <div>{row.name}</div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {!jobTitles.length && <EmptyTableMessage>Nenhum cargo cadastrado</EmptyTableMessage>}
            </Paper>
        </>
    );
};

export default withRouter(JobTitleTable);