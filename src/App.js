import React, {useEffect} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    withRouter,
} from "react-router-dom";
import styled from "styled-components";
import AppBar from './components/AppBar';
import People from './components/People';
import PeopleForm from './components/People/form';
import Competence from './components/Competence';
import CompetenceForm from './components/Competence/form';
import Department from './components/Department';
import DepartmentForm from './components/Department/form';
import JobTitle from './components/JobTitle';
import JobTitleForm from './components/JobTitle/form';
import Login from './components/Login';
import {getLoggedUser} from "./localStorage";

const PageWrapper = styled.div`
  padding: 20px 20px 0 20px;
`;

const AppWrapper = withRouter(({children, location, history}) => {
    useEffect(() => {
        const loggedUser = getLoggedUser();
        if (!loggedUser && location.pathname !== '/nova-conta') history.push('/login');
        if (loggedUser && !location.key) history.push('/funcionarios');
    }, [location.pathname]);

    return <PageWrapper>{children}</PageWrapper>;
});

const App = () => {
    return (
        <Router>
            <div>
                <nav>
                    <AppBar/>
                </nav>

                <Switch>
                    <AppWrapper>
                        <Route exact path="/login">
                            <Login/>
                        </Route>

                        <Route exact path="/funcionarios">
                            <People/>
                        </Route>
                        <Route exact path="/nova-conta" component={PeopleForm}/>
                        <Route exact path="/novo-funcionario" component={PeopleForm}/>
                        <Route exact path="/funcionarios/:id" component={PeopleForm}/>

                        <Route exact path="/competencias">
                            <Competence/>
                        </Route>
                        <Route exact path="/nova-competencia" component={CompetenceForm}/>
                        <Route exact path="/competencias/:id" component={CompetenceForm}/>

                        <Route exact path="/setores">
                            <Department/>
                        </Route>
                        <Route exact path="/novo-setor" component={DepartmentForm}/>
                        <Route exact path="/setores/:id" component={DepartmentForm}/>

                        <Route exact path="/cargos">
                            <JobTitle/>
                        </Route>
                        <Route exact path="/novo-cargo" component={JobTitleForm}/>
                        <Route exact path="/cargos/:id" component={JobTitleForm}/>
                    </AppWrapper>
                </Switch>
            </div>
        </Router>
    );
};

export default App;