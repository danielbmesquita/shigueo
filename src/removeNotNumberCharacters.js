const removeNotNumberCharacters = value =>
  value ? value.replace(/[^0-9]/g, '') : value;

export default removeNotNumberCharacters;
