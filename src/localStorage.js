export const loggedUser = 'loggedUser';
export const employees = 'employees';
export const competences = 'competences';
export const departments = 'departments';
export const jobTitles = 'jobTitles';
export const EMPLOYEE_COMPETENCES = 'employeeCompetences';

const generateId = () => Math.floor((Math.random() * 100000000) + 1);

// loggedUser
export const getLoggedUser = () => {
    const loggedUserFromLocalStorage = localStorage.getItem(loggedUser);
    return loggedUserFromLocalStorage ? JSON.parse(loggedUserFromLocalStorage) : null;
};

export const setLoggedUser = object => localStorage.setItem(loggedUser, object);

// departments
export const listDepartments = () => {
    const departmentsFromLocalStorage = localStorage.getItem(departments);
    return departmentsFromLocalStorage ? JSON.parse(departmentsFromLocalStorage) : [];
};

export const addDepartment = name => {
    const currentDepartments = listDepartments();
    const newDepartment = {
        id: generateId(),
        name,
        creationDate: new Date(),
        updateDate: new Date(),
    };
    currentDepartments.push(newDepartment);
    localStorage.setItem(departments, JSON.stringify(currentDepartments));
};

export const updateDepartment = (idToUpdate, name) => {
    const currentDepartments = listDepartments();
    const index = currentDepartments.findIndex(item => item.id === idToUpdate);
    currentDepartments[index] = {
        ...currentDepartments[index],
        name,
        updateDate: new Date(),
    };
    localStorage.setItem(departments, JSON.stringify(currentDepartments));
};

export const deleteDepartment = id => {
    const currentDepartments = listDepartments();
    const index = currentDepartments.findIndex(item => item.id === id);
    currentDepartments.splice(index, 1);
    localStorage.setItem(departments, JSON.stringify(currentDepartments));
};

// job titles
export const listJobTitles = () => {
    const jobTitlesFromLocalStorage = localStorage.getItem(jobTitles);
    return jobTitlesFromLocalStorage ? JSON.parse(jobTitlesFromLocalStorage) : [];
};

export const addJobTitle = name => {
    const currentJobTitles = listJobTitles();
    const newJobTitle = {
        id: generateId(),
        name,
        creationDate: new Date(),
        updateDate: new Date(),
    };
    currentJobTitles.push(newJobTitle);
    localStorage.setItem(jobTitles, JSON.stringify(currentJobTitles));
};

export const updateJobTitle = (idToUpdate, name) => {
    const currentJobTitles = listJobTitles();
    const index = currentJobTitles.findIndex(item => item.id === idToUpdate);
    currentJobTitles[index] = {
        ...currentJobTitles[index],
        name,
        updateDate: new Date(),
    };
    localStorage.setItem(jobTitles, JSON.stringify(currentJobTitles));
};

export const deleteJobTitle = id => {
    const currentJobTitles = listJobTitles();
    const index = currentJobTitles.findIndex(item => item.id === id);
    currentJobTitles.splice(index, 1);
    localStorage.setItem(jobTitles, JSON.stringify(currentJobTitles));
};

// competences
export const listCompetences = () => {
    const competencesFromLocalStorage = localStorage.getItem(competences);
    return competencesFromLocalStorage ? JSON.parse(competencesFromLocalStorage) : [];
};

export const addCompetence = name => {
    const currentCompetences = listCompetences();
    const newCompetence = {
        id: generateId(),
        name,
        creationDate: new Date(),
        updateDate: new Date(),
    };
    currentCompetences.push(newCompetence);
    localStorage.setItem(competences, JSON.stringify(currentCompetences));
};

export const updateCompetence = (idToUpdate, name) => {
    const currentCompetences = listCompetences();
    const index = currentCompetences.findIndex(item => item.id === idToUpdate);
    currentCompetences[index] = {
        ...currentCompetences[index],
        name,
        updateDate: new Date(),
    };
    localStorage.setItem(competences, JSON.stringify(currentCompetences));
};

export const deleteCompetence = id => {
    const currentCompetences = listCompetences();
    const index = currentCompetences.findIndex(item => item.id === id);
    currentCompetences.splice(index, 1);
    localStorage.setItem(competences, JSON.stringify(currentCompetences));
};

// employees
export const listEmployees = () => {
    const employeesFromLocalStorage = localStorage.getItem(employees);
    return employeesFromLocalStorage ? JSON.parse(employeesFromLocalStorage) : [];
};

export const addEmployee = ({name, document, email, password, idDepartment, idJobTitle}) => {
    const currentEmployees = listEmployees();
    const newEmployee = {
        id: generateId(),
        name,
        document,
        email,
        password,
        idDepartment: idDepartment || '',
        idJobTitle: idJobTitle || '',
        creationDate: new Date(),
        updateDate: new Date(),
    };
    currentEmployees.push(newEmployee);
    localStorage.setItem(employees, JSON.stringify(currentEmployees));
};

export const updateEmployee = ({idToUpdate, name, document, email, password, idDepartment, idJobTitle}) => {
    const currentEmployees = listEmployees();
    const index = currentEmployees.findIndex(item => item.id === idToUpdate);
    currentEmployees[index] = {
        ...currentEmployees[index],
        name,
        document,
        email,
        password,
        idDepartment: idDepartment || '',
        idJobTitle: idJobTitle || '',
        updateDate: new Date(),
    };
    localStorage.setItem(employees, JSON.stringify(currentEmployees));
};

export const deleteEmployee = id => {
    const currentEmployees = listEmployees();
    const index = currentEmployees.findIndex(item => item.id === id);
    currentEmployees.splice(index, 1);
    localStorage.setItem(employees, JSON.stringify(currentEmployees));
};

export const listEmployeeCompetences = () => {
    const employeeCompetencesFromLocalStorage = localStorage.getItem(EMPLOYEE_COMPETENCES);
    return employeeCompetencesFromLocalStorage ? JSON.parse(employeeCompetencesFromLocalStorage) : [];
};

export const addCompetenceToEmployee = (idCompetence, idEmployee) => {
    const employeeCompetences = listEmployeeCompetences();
    employeeCompetences.push({
        id: generateId(),
        idCompetence,
        idEmployee,
    });
    localStorage.setItem(EMPLOYEE_COMPETENCES, JSON.stringify(employeeCompetences));
};

export const deleteEmployeeCompetence = id => {
    const employeeCompetences = listEmployeeCompetences();
    const filtered = employeeCompetences.filter(item => item.id !== id);
    localStorage.setItem(EMPLOYEE_COMPETENCES, JSON.stringify(filtered));
};
